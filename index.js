#!/usr/bin/env node

const program = require('commander');       //设计命令行
const download = require('download-git-repo');      //github仓库下载
const inquirer = require('inquirer');       //命令行答询
const handlebars = require('handlebars');       //修改字符
const ora = require('ora');         //命令行中加载状态标识
const chalk = require('chalk');     //命令行输出字符颜色
const logSymbols = require('log-symbols');      //命令行输出符号
const fs = require('fs');
const request = require('request');
const { resolve } = require("path");
const install = require("./utils/install");

console.log(chalk.green(`
                           tengyu cli 命令
    ------------------------------------------------------------
       tengyu init <template name> projectName  |  初始化项目 
       tengyu -V                                |  查看版本号    
       tengyu -h                                |  查看帮助      
       tengyu list                              |  查看模板列表  
       tengyu download                          |  下载zip模板  
    ------------------------------------------------------------
`));
// 可用模板
const templates = {
    'react-npm-template': {
        url: 'https://gitee.com/bingtengaoyu/reactAntd',
        downloadUrl: 'https://gitee.com:bingtengaoyu/reactAntd#master',
        description: 'react基础模板'
    },
    'vue-tools': {
        url: 'https://gitee.com/bingtengaoyu/vueTools',
        downloadUrl: 'https://gitee.com:bingtengaoyu/vueTools#master',
        description: 'vue常用组件'
    }
}

// tengyu -V|--version
program.version('1.0.0');  // -v|--version时输出版本号0.1.0

// tengyu init <template> <project>
program
    .command('init <template> <project>')
    .description('初始化项目模板')
    .action((templateName, projectName) => {
        console.log(templateName, templates);
        let downloadUrl = templates[templateName].downloadUrl;
        //下载github项目，下载墙loading提示
        const spinner = ora('正在下载模板...').start();
        //第一个参数是github仓库地址，第二个参数是创建的项目目录名，第三个参数是clone
        download(downloadUrl, projectName, { clone: true }, err => {
            if (err) {
                console.log(logSymbols.error, chalk.red('项目模板下载失败\n   只能下载list列表中有的模板'));
                console.log(err);
            } else {
                spinner.succeed('项目模板下载成功');
                //命令行答询
                inquirer.prompt([
                    {
                        type: 'input',
                        name: 'appid',
                        message: '请输入appid',
                        default: ''
                    },
                    {
                        type: 'input',
                        name: 'name',
                        message: '请输入项目名称',
                        default: projectName
                    },
                    {
                        type: 'input',
                        name: 'description',
                        message: '请输入项目简介',
                        default: ''
                    },
                    {
                        type: 'input',
                        name: 'author',
                        message: '请输入作者名称',
                        default: ''
                    }
                ]).then(answers => {
                    //根据命令行答询结果修改package.json文件
                    let packsgeContent = fs.readFileSync(`${projectName}/package.json`, 'utf8');
                    let packageResult = handlebars.compile(packsgeContent)(answers);
                    fs.writeFileSync(`${projectName}/package.json`, packageResult);
                    console.log(packageResult)
                    fs.writeFileSync(`${projectName}/config.js`, `module.exports = ${JSON.stringify(answers)}`);

                    console.log(logSymbols.success, chalk.green('项目初始化成功，开始下载依赖...'));

                    install({ cwd: `${resolve('./')}/${projectName}` }).then(data => {
                        console.log(logSymbols.success, chalk.green('项目依赖下载成功！'));
                    });

                    //用chalk和log-symbols改变命令行输出样式
                })
            }
        })
    })

// 下载zip模板
program
    .command('download')
    .description('初始化项目模板')
    .action((templateName, projectName) => {
        inquirer.prompt([
            {
                type: 'input',
                name: 'project_name',
                message: '请输入项目名称',
                default: 'tengyu-template'
            },
            {
                type: 'list',
                name: 'template_name',
                message: '请选择需要下载的模板',
                choices: [
                    'react快速开发模板',
                    'vue工具集'
                ],
                default: 'react-npm-template'
            }
        ]).then(answers => {
            let url = ''
            switch (answers.template_name) {
                case 'react快速开发模板':
                    url = templates['react-npm-template'].url;
                    break;
                case 'vue工具集':
                    url = templates['vue-tools'].url;
                    break;
                default:
                    url = templates['react-npm-template'].url
            }

            function downloadFile(uri, fileName, callback) {
                var stream = fs.createWriteStream(fileName);
                request(uri).pipe(stream).on('close', callback);
            }

            downloadFile(url, `${answers.project_name}.zip`, function () {
                console.log(logSymbols.success, chalk.green(`${answers.template_name}下载完毕！`));
                return
            });
        })
    })

// tengyu list
program
    .command('list')
    .description('查看所有可用模板')
    .action(() => {
        console.log(chalk.green(`
                              tengyu 模板
            -----------------------------------------------
                 react-npm-template   react快速开发模板  
                 vue-tools            vue工具集
            -----------------------------------------------
        `))
    })

program.parse(process.argv);